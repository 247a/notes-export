psudo

Running file on the application we see that the application is a elf file and is for a 64 bit arm system.



![8b947fcc170b6e26912bc76a3864b325.png](../../../_resources/4e0ad236ce28417da1ca8f52c679031b.png)


Further by running strings, on the application we find that the application is packed with UPX.


![8c35a371df9a0eeb17e911be7092a736.png](../../../_resources/6d912a6f4b2f4387bb2e12835d03c38f.png)


Inorder to unpack we can use the preinstall appilication in kali called upx.


![2d63f369a91732fad56783f4aea8f64e.png](../../../_resources/55170fd7a817420aa76219bb23ca130d.png)


rerunning strings we see we likely have genuine application


![913ec92bafb842efd19a690dcbf2982d.png](../../../_resources/68816ad8c6ff4cee904d6d30fb346620.png)


Nice fake flag, the base64decode of the value in the HTB{} is “you're still not trying hard enough”
Using the tool cutter, which is a wrapper for radare2 we find that the main function is located at: 0x004004b0


![5d4fd8b603b0b831be6b68b579a791e0.png](../../../_resources/fc26c3746f5b480db995ab3dc2f6c871.png)


# Post Pack

## emulate
sudo apt install qemu-user-static
qemu-aarch64-static -g 12345 ./pseudo

## Connect debugger
sudo apt install gdb-multiarch
gdb-multiarch ./pseudo
target remote localhost:12345


No aslr is nice :)
means locations in GHidra match in mem.



add watch on mem access:
rwatch *<mem address>
eg: rwatch *0xdeadbeef




flow
Main() 0x00000000004004b0

