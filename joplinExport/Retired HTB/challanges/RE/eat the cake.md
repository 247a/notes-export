eat the cake

we get a exe
running files on the exe we see that it is a x86 microsoft exe which is upx packed.


![978b07b8b1d37ddbfcb661f7524267c9.png](../../../_resources/0109c3cb14c242c79ec22894143290a0.png)


hence we can use upx to unpack


![28c3747d91d451a7a6584d7f55695d49.png](../../../_resources/677ca82cbf884baf8c83cdaa22abdad0.png)


However wehen we run it then failes to execute.
Hence we can use PE Explorer to extract the PE file

Useing Ida we locate the string which states congrageulations

Next we can put a brake point at teh start of teh function

Entering:
1234567890
and 
abcdefghijklmno

we see we call a function (sub_D12F0)
this fuction preforms some validation, on return we do further validation, below is a table of the validations


| click me | extracted value | click me | extracted value |
| --- | --- | --- | --- | --- | --- |
| 1 |  | a | 68h h |
| 2 |  | b | 40h @ |
| 3 |  | c | 63h c |
| 4 |  | d | 6Bh k |
| 5 |  | e | 74h t |
| 6 |  | f | 68h h |
| 7 |  | g | num 3 |
| 8 |  | h | 70h p |
| 9 |  | i | 61h a |
| 0 |  | j | 72h r |
|  |  | k | 61h a |
|  |  | l | 64h d |
|  |  | m | num 1 |
|  |  | n | 24h $ |
|  |  | o | 45h E |

now if we test this
