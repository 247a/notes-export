dsym

In this we get two files.
Lets see what they are with files.


![bcdc464d8b91559c2e963b779da7ba8c.png](../../../_resources/eae91354f2454aad8bd61ecd33f9d395.png)


![e5615b4583740d0ba8b35026db93f27f.png](../../../_resources/a9d8e95d21b84350aac15dd1d4002687.png)





Loading  Ida free we find the dunnoWhatIAm is a lib
Aslo that the main is empty of getme

Also that the fucntion notme is never called.
There for in _init_proc we see that there is a branch where if rax is set rax is called.


![7cc8723bc7a374137b168fbee85a9fb1.png](../../../_resources/cc7602d23db0470f832f221c2cd290eb.png)


Hence we can set Rax to the ptr of notme.
In this function we get an out put of


![92a09e8f7a9c24f16912f05b0b43ef80.png](../../../_resources/6a1b2d4e0f2c463dbdc58ad859b3423b.png)


which is gained buy pushing to the stack. 
Printing the message upto the “:”
Then going though a decription loop
to print out a randome hex number


![b0f734ff8804cd8744403747c0792616.png](../../../_resources/ad92fd8764f443e3b4a163fd08be8920.png)


If we paste it into an HEX to  ASCII converter we get.
UGO{l0h_e34yyl_t0g_z3}

This is not right.
As the algorithem is a XOR algrithom, and the first three chars should be HTB.
We can use these to get the correct key.
For this we identify the xor'ed letters which made UGO.
which are: 0x2cf 0x2dd 0x2d5
if we xor those with the ascii of HTB which is 0x48 0x54 0x42
For each letter we get the output of 0x287 0x289 0x297

Patching the binarys decode key we get the new output of.


![0446f0c7577b401c91033b74d3d50c25.png](../../../_resources/47f1a324d3e94b8eb07673b969857a93.png)



