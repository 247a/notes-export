find the secret flag

we get a bin file
running file on it we get


![26f50b09f22567cff20337e90c90821a.png](../../../_resources/0c395bc16a124d66b80157b9b958a475.png)


running strings we see som interesting strings 


![a9dbacf43cfbab19c778a08c9bc16962.png](../../../_resources/129d2a9a7b0948bf8a7ad9274164c017.png)


Using ida we can see that we have to create a file /tmp/secret and use that as the input


![affe15069e0a5d9bf4a7cd2170df399f.png](../../../_resources/d309b27ecbc349ef901282a308c9d62a.png)


Hence i use the initial input of 1234567890abcdefghijklmnopqrstuvwxyz as a starting point.
after intial debugging it was apparent that a hex editor was required to build the input file


It also looks like a rand function is used
And that a pramater may be used



33242f052326332405331d2433221d332f0e