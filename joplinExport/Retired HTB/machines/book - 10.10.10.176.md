book - 10.10.10.176

So to begin we have a http server and a ssh server


looks like the web server takes a long time to respond.
but there is a sign in page


![7a870dc67a2ed95cc7d8e522eb095047.png](../../_resources/b32ab70f733b44129f49ca8d81232462.png)


after makeing an account and logging in we get


![58ee82df4e3db04cbb2099fc4f44019d.png](:/07a5c111021643c2ad2923a9c746fb5d)


Navigateing the site we see a list of books


![da221064b76f450ae65a17f24e752795.png](../../_resources/f47dad85687346e284cd5dc7526b5d4d.png)


the images give a link to a download via a get request


![a626dec37688507d2f3dfc840286188e.png](../../_resources/ecd678e759d1448281ddb309569a00df.png)


I suspect sqli

checking the gobuster scan initally was empty, which is odd as there where several pages which should have been listed that wernt.
Re running auto recon we then see more results including an admin site.


![dbf430cd79696c5a1c004a1ff9599fa7.png](../../_resources/2eb69ebdc32d46b8a13d534695604218.png)



Attempting login we get


![5cf27663c216cbe1e1b3b8b4d2e84351.png](../../_resources/dd432c6b54be470fbbc1fb61bc14c4bc.png)




