control - 10.10.10.167

Initial nmap shows there is a web server and a mysql server


![75105bd3c2c85cd159942a8ed6bbc117.png](../../_resources/be8c7edd11cf4a338155cc44feace798.png)


The inital webpage has links to both an admin and a login page


![3c3467ec229e78aa3565232c2de07212.png](../../_resources/96f4c7d2abd3454b82b4ddcdcaf4df12.png)


When vieing the source we see a comment


![dc1bb488ff91dae41957107a5dd4f8a5.png](../../_resources/82402dee39a045b89907013ef903ee70.png)


When attempting to access /myfiles failed


![ed37610b885fbca4e1fee8c132aa8307.png](../../_resources/6acfac078dfc4d2481d2386182eca5dd.png)


Accessing the admin page we see the following error


![8b3e54262fa7f793a508e235dc6cc3b7.png](../../_resources/cf20596a226147f1980de6c137126f82.png)


If we intercept and modifiy to include the X-Forwarded-For header which lets the server know where the request came from and is used by proxies 


![3a890927035a86001a154235b3cd6958.png](../../_resources/7dacb788242d4732bd20c6e13fe16e1e.png)


and we get access


![cb4a2c58bbb2b20dfbc15686c74df2b5.png](:/44c9ff372f14436d8349f1152e4b16e4)







likely SQLI on view/create product/category
likely SQLI on view/create product/category