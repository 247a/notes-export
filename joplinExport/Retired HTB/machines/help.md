help

On the initial autorecon scan we found ssh and 2 http servers, one on port 80 and one on port 3000.

On port 80 we see the defualt apache page when accessed via IP
![e2b002e27abf2906557aa178863c12ae.png](../../_resources/e00c25b6258f4f88adefb9ac24a54e98.png)

On port 3000 we recive a JSON reply which indicates a JSON api. We also get a user Shiv and a message to find some credentials.
![4b6b1e18070a61f104b885ddda86c75e.png](../../_resources/e2da4d532ae84364858096526969dd37.png)

Further we see that the port 80 is hosted using Apache/2.4.18 via the _patterns.log.

Looking at gobuster on port 80 we see:
![4646531a0921410d760dbb9982004271.png](../../_resources/a54fb95746f14c5d85585a2e9e6f610a.png)

Navigating to the `support` folder we see what appears to be a broken website.
![79a700c102433ebc2e0d9cfa157f6ab1.png](../../_resources/9ff058c142544e01858aa294389638c4.png)

however there are links to a `help.htb` so i added the line to my host file.

Once added we see the page works.
![d6e5cceebf7cc2ea02830a8d5a17ccc0.png](../../_resources/4d7c95be755e409a80e7f1d40e63bbe5.png)
