postman - 10.10.10.160

performing an nmap scan we can see a ssh server, a http server a redis server and a https webmin server



![2aa8fb82f75a24636716160e8db6879a.png](../../_resources/3dde962c8ad74488870fbe1e19fa1b2c.png)



![73b3429309e680a636e2a19152133150.png](../../_resources/b3a2391ad09e409fbd4a7ba6ee088ec7.png)



on the web server we see a simple site


![0aff04491941b8ed4f450ace208315f7.png](../../_resources/d046c9cdcc3f4eefbc29bca6dc80f433.png)


when accessing the webmin page via http  we get an error page stateing to use https


![5cceda538f6839f7b5fcb6bac60505c0.png](../../_resources/4de6bf96b45a40ad97aec0f1a2ea1e5b.png)


on useing https we see a webmin login


![2fa4e5af5f68136cce13719ea82fb40b.png](../../_resources/9c43a8b31d6842698011b7a34bc75356.png)


useing searchsploit we may get a local file inclusion


![39a8c8b33f2aae49abcf675ca710df58.png](../../_resources/e949089f5751498fbbe209eef353201b.png)


Attempting to connect to the redis server we are alowed in unauthenticated.


![88392cf8c0d3f23acc51a15174b56c04.png](../../_resources/1d9c5e395a2a4e77898cfa4cd8085b31.png)


hence i create a ssh key as we don't know the version of openssl i will just use rsa rather than my usual favorate of ed25519


![823cf5a8188c7324d7d055d914b3a19f.png](../../_resources/9e6599da51c24cc9aaf8d641cb317f4d.png)


and registered it locally 


![c4e03a39cf87bec7b54f09c2594d98e5.png](../../_resources/6de96aeea70d4c549aeee4341338f8ab.png)


next we load the public key into a form which redis will use



![0c10883a21fc5e5c897de3a20755f23e.png](../../_resources/0d308ecd7e6646f996e96c70592a1a74.png)






