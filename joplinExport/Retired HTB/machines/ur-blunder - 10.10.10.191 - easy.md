ur-blunder - 10.10.10.191 - easy

To begin autorecon was ran to identify possible open ports and the running services.

From this we found a http server.



![c430248f389fe8571e375c24e2451ad2.png](../../_resources/673c3675a58e481392b9c02ef2fd9a85.png)

Looking at the output of the whatweb stage we err that the server is reported as Ubuntu running Apache 2.4.41

Addtionally in the X-powered-By header we see that there is a referance to bludit. When searched for it is referacned as a CMS https://www.bludit.com/

Looking at the output of the gobuster stage we can see there is a license, an admin page, and an about page.

The license and the about did not revail any info how ever the admin page reinterated the bludit usage.

also we find a todo.txt which contained:
```
-Update the CMS
-Turn off FTP - DONE
-Remove old users - DONE
-Inform fergus that the new blog needs images - PENDING
```


This gives us a potential user.
Following this we can use a new tool crewl to build a wordlist with the command `cewl <ip> -w <outfile>`
![31e1da490ba4dbf21dc2129b9a180400.png](../../_resources/8521a45b77284348b366a3d7415a0e36.png)

next searching on exploitdb we find an auth bruteforce mitigation bypass, and as we now have a wordlist we can attempt to dictionary attack our way in.
![bc14200abfe063abc71286c8b1539b24.png](../../_resources/e4eacde02c604b5fa0583d5115e4a0ab.png)
for the exploit i was required to install docopt, httpclient and http-cookie
![355878ce41381daee1b74dec26a783d7.png](../../_resources/bf0a2639e8e1419dbb072f78741c471b.png)
![21d4e8016d2682d525b78a3ee65a4b62.png](../../_resources/bd76020650b24859b73ab15001d72fa8.png)

![dd9fdbc68e361bfe6b374bcba74ae635.png](../../_resources/bf4f3aa316064ca5bbd165f7ff030369.png)
attempting teh exploit we see
![8009b88e4c24cedec36ec222ef552a27.png](../../_resources/8d28bccc4d104f86b8fe8c68b9d02967.png)
however no password worked.
retrying against the `fergus` user with the command `ruby 48746.rb -r http://blunder.htb -u fergus -w cewl_output.txt`
with this we see the password is `RolandDeschain`
![824360b5d16c7ebc1cb40310b6d59013.png](../../_resources/099a9c1b49ae471dba084900b4a215c8.png)

With this we can login as fergus
Attempting to create a page with php failed as it was filtered and the php became a html comment
![6fc0ec11210a0b6aa0177049f00ae15b.png](../../_resources/088a6841c58b46a79cd0901a3d8372ea.png)
going back to expolitdb we see the following exploit
![3e59ff62f5eb4cd1c8f42c06c276de3d.png](../../_resources/bb41a576c9b342ad9e94340a73ca9834.png)

using the exploit for a reverse shell we see
![b7845c5674e2c00732e83183a292db8d.png](../../_resources/9de328a0f3ad47e487b89405418a5afb.png)
![1f8453ce3fb4c7e1f85bd4e82f97d2eb.png](../../_resources/dc76b1a559db464b87c1d67cb2820930.png)

looking round the file system i noticed an ftp folder.
This contained the following
![d24401cce79581b50324f12dc63ebd15.png](:/4afc5dd7b45f46549b061f93fbfd890c)
which indicates the config files may be inportant as well as some other files located else where.
I also located home folders for hugo and shaun.
With shaun having a file indicateing sudo.


the config file was a binary file and the config.json contained
```
{
  "squadName": "Super hero squad",
  "homeTown": "Metro City",
  "formed": 2016,
  "secretBase": "Super tower",
  "active": true,
  "members": [
    {
      "name": "Molecule Man",
      "age": 29,
      "secretIdentity": "Dan Jukes",
      "powers": [
        "Radiation resistance",
        "Turning tiny",
        "Radiation blast"
      ]
    },
    {
      "name": "Madame Uppercut",
      "age": 39,
      "secretIdentity": "Jane Wilson",
      "powers": [
        "Million tonne punch",
        "Damage resistance",
        "Superhuman reflexes"
      ]
    },
    {
      "name": "Eternal Flame",
      "age": 1000000,
      "secretIdentity": "Unknown",
      "powers": [
        "Immortality",
        "Heat Immunity",
        "Inferno",
        "Teleportation",
        "Interdimensional travel"
      ]
    }
  ]
 ```
 dumping the config file and checking it with file we see it is a zip. Once unzipping we see that there is a wav file within.
 ![70b96ee77210394edbb73027db6b5fc3.png](../../_resources/a008c8d0c7d74f7ea3e55b2de849e974.png)
 
 Looking at the new install which was mentioned in the todo file we see
 ![96c3e7a21205b8dd141251262929083b.png](../../_resources/719150d28b544525862e3b28a849f78f.png)
 
 cracking the password we see the password is `Password120`
![8d604aa91105de75c08987a62f0893bd.png](../../_resources/5e24f07780d04009ac8244a699bb2082.png)
We can now login as hugo
![b08c2f451446e381db3443b303031ab5.png](../../_resources/c64216737f784e9aa89aac776883e369.png)
At this point i needed to upgrade my shell.
![1e197b846b7e243330df62081925d7b8.png](../../_resources/8b264ff8257545b195b2516b64dd4103.png)
Then i could use `sudo -l`

<!--Img missing-->

From this as we can use bash as all users bar root instantly i think of the recent sudo bug in which specifing an invalid user id allows root.
we can do this with `sudo -u#-1 /bin/bash`
![f795ac9f2a11aeae64cdea1b60a2e860.png](../../_resources/e368ab4e488b4ae8908dcb5ef8c090e0.png)
