ur - Nest - 10.10.10.178


So for got to save .... so few screen shots

Nmap found smb

Connecting with:
python3 /home/htb/Desktop/tools/impacket/examples/smbclient.py -target-ip 10.10.10.178 guest@HTB-NEST

I found a usefull file in the Data share. under /Shares/templatesHr/WelcomEmail.txt 
this gave creds to Tempuser

Username: TempUser
Password: welcome2019

connecting with:

python3 /home/htb/Desktop/tools/impacket/examples/smbclient.py -target-ip 10.10.10.178 TempUser:welcome2019@HTB-NEST

This also indicated we should be able to acess the IT area of the Data share

with this we can access /IT/Configs/RU scanner/RU_config.xml


```xml
<?xml version="1.0"?>
<ConfigFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Port>389</Port>
  <Username>c.smith</Username>
  <Password>fTEzAfYDoz1YzkqhQkH6GQFYKp1XY5hm7bjOP86yYxE=</Password>
</ConfigFile>
```

and /IT/Configs/Notpad++/config.xml
which pointed at some interesting direcotries



![2ea4a673307f27ab675f4872073069a9.png](../../_resources/131215a8d5df45ed8223ef7b4b3fc3a3.png)



from here when we access Secure$/IT we get access denied however Secure$/IT/carl is allowed



![19dca537bd6e12cc46b3210c4d529b2e.png](../../_resources/2f5da7c33c0143a3b2d4aa58b5720101.png)



After pulling teh code, and tweaking it to cat out the plan pasword we get


![6dcb5340e249dc7f1767e4aaf3e78012.png](../../_resources/81c97fcd11a9440d859778b62391605b.png)



connecting with:
python3 /home/htb/Desktop/tools/impacket/examples/smbclient.py -target-ip 10.10.10.178 c.smith:xRxRxPANCAK3SxRxRx@HTB-NEST

we can now get user directory of c.smith


![1febbd2569f7f5901c980f818d2a9200.png](../../_resources/cfea29d2433e4b7792529a1bcdb352aa.png)



Further inside the HQK Reporting is some interesing files.



![cb556622bc7301d3d0bb441f2d4d1519.png](../../_resources/685185652570481e8a8d5bcdbfdfa20b.png)



Especially as this is what reports on the other port.


Using smb client we can find the alternate data strems of Debug Mode Password


![1caf3f31732f3b120c1315f8cff9233d.png](../../_resources/0045bdbcda734077b0fb54faaa34f427.png)



![c8fd4d1c53e17b913c1d2d28a5a1adce.png](../../_resources/0e8a4f48b98b4e61ac1204a88088d317.png)



Using the alternate data stream



![f5c8d7fc41d6124cc99b9103e315cdec.png](../../_resources/4b8929cadf444a0682394b4f9a93d3a4.png)



![980c05723d37bedbb9363d256c9e680a.png](../../_resources/a1862dfbfde74b83ae4a6905c85ec134.png)



Next we will access the Hqk service


![8f59764d2df6fc5ed15bdf825a597f8c.png](../../_resources/d6a5502716784d239e2855d71068404c.png)


and use the debug password


![262cf1e6c5c541c0bcbd416f1c7d04f2.png](:/f428396637c84e6ba96c2fdf300d060b)



![f4019919a3b312f7b237464a904540e9.png](../../_resources/83a2832791b54106ba38894be4ccbcb8.png)





navigating round the server we find 


![1a2095f7998fe2688d37171d5cccd7d1.png](../../_resources/fbddd8e4d6ea45e195db5486f494d157.png)



back to smb we can get a copy of the hqkldap.exe


![97f1d3beb219eb398d398e52ec70cbc9.png](../../_resources/038eaed432ec49cebd98ce4f04c973f4.png)



So we have a HqkLdap file. so i decide to decompile it with ILspy on my windows VM.(as no .net decompilers worked on linux)


![7ac32a86d2444c883b23411b3ec1cabb.png](../../_resources/5a7b605ddebf4ffd9255e350b274f7ee.png)


stripping the source down to the encriptor and decryptor class i made the app decrypt the pass


![f57434f98cae959685044bb52a8bdcb2.png](../../_resources/6e9d80295c854604958e29f5f6b633af.png)



![bc48a2a4b8bbe835d8c4ed401c4c8c6b.png](../../_resources/23eef4a532ed41d59e3e461117d04b53.png)





and we can login with smb with this


![938ca64559b6186daedf5e4a6ad1a1e8.png](../../_resources/6268c7b129b947a88ccf213a6eaa86b3.png)


Then under the users share we can see the amin folder. which has a link file


![6dc84a144687afa89123c6a29ee61e7d.png](../../_resources/a3ae04b6b6de4640b303c7d210bbd9d5.png)


opening the lnk file in cyberchef shows the next location to look


![5d3a1bfb731ca3ed130f8ac3fb24d82a.png](../../_resources/98e47727d3174320ba09e5629bd76d11.png)


so useing teh c$ share we get




![9988eb08342f5385097d8c81b9ed7211.png](../../_resources/4b03947250e145ea9d1d19d140dca0b7.png)




