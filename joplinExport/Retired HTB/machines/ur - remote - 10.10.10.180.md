ur - remote - 10.10.10.180

Looking at the output we  see that there is an ftp server which allows annomus login.
However there is nothing stored on the ftp


Additonally there is an nfs share which is listening on port 111.
Looking at the show mount we get

```
Export list for 10.10.10.180:
/site_backups (everyone)
```


therfore we can mount the nfs share.


![165031e24b82b5cc6aedd0628c22268a.png](../../_resources/e1f06c60a4ef4a2ab9df61e587140ee3.png)



navigating the website we see 


![77f74a925c3dc9354c0772f773b490f5.png](../../_resources/b7cc35a379394aa582cc663262f0ccfd.png)


which leads to 


![c66287016f1f512e154f4200d5039d65.png](../../_resources/f6b49b0ffd3d431388e778d24a9e289e.png)




Looking at the files we found on the nfs for a username or password we get


![612d118c092a573355a3c3342adb9056.png](../../_resources/13b4b8b4655e4689b0b8163a739b2549.png)


which looks like logs leaking logins.

Seaching that file we get the folloing login usrnames:
admin@htb.local
Umbracoadmin123!!
ssmith@htb.local

As the users look like the format of an email on two of them and the login page says it should look like an email.
The ‘Umbracoadmin123!!’ looks as though someone typed the password in the username field.

trying said password with both users we get
![6736ebe4470b1f2275c9a6e93b300b1a.png](../../_resources/6b2b394df3cb46b68e6bb4fa9d1c4c96.png)

![32243f40e9835b4ecd9598363de57ef0.png](../../_resources/bfe330bdae8b4d3a952d22702db88065.png)




So that didnt work.
however lower down there is this error message


![1bd5a32ba30f739d07c5647fec64a176.png](../../_resources/b96688318b12406eb548a7c161022bea.png)


so we have an sdf file 
using strings we can attept to read the passwords


![c70cc15b55c746d6a3f8104496ac26e2.png](../../_resources/48670fe18a2c4ca59aee6226047afd65.png)



This appers like b8be16afba8c314ad33d812f22a04991b90e2aaa is the hash to the admin user.
And that the hash is a sha-1 hash


![9c6bce60a8d57afb98b6190b58acbd3f.png](../../_resources/26570ff3c78b459d84f7bee4fc0a9110.png)



<!-- image missing -->



usint this new password for the login witht eh admin email from before we gain access



![04060af21a8420601ad0cd082a0ad9d7.png](../../_resources/b2a8d7c561cc44d49a2bc1f856c64563.png)



Looking on searchspoilt for an exploit for umbraco we get 


![6222ed3dd7bb11042e1b258eccd12fe6.png](../../_resources/dc076396527b4fb19c7c10c1c966e322.png)



![574c952d5bd94f11c679511a727b9472.png](../../_resources/2634ef161d944d9cbe47cd7e677b955f.png)


after modifiying to support a script and using powershell as a revers shell we gain access


![9a12a00d364a5364580b12acce35a8a4.png](../../_resources/7675557e476e4fba9de722e07ccbba85.png)



![34b4f97a0a4efc750474c01dcac5d026.png](../../_resources/05191b9fea2e4e8cbe2d8dd81ac007cf.png)




![2a2c18f8840441075f1e4d4b2a2c0b0c.png](../../_resources/606a869603fa4090af603c26682f899f.png)




using winpeasany we get 



![cfb0ff9ec00236490525f69afadcdc19.png](../../_resources/e26d326fd5f44d07ad19a56ee65599d2.png)


With this we can use the instructions listed at: https://book.hacktricks.xyz/windows/windows-local-privilege-escalation#serviceshttps://book.hacktricks.xyz/windows/windows-local-privilege-escalation#services



![02241ffd3697a9343cb916c1939d0cb1.png](../../_resources/f00d5a323a464e19a452566b34ab8a17.png)



First we can upload netcat


![5fae007a384c3b75d55a71b55e76c8ad.png](../../_resources/2d7ad78f577d45d793e5c3e4771b0054.png)



![61665566598603c25e458ea3c842657a.png](../../_resources/9c96fc95da3646fd9e28d26428121318.png)



<!-- image missing -->



![464078d469067071f0d03be3b1639f5e.png](../../_resources/d885c60b12544e84b73e1a47644696a7.png)


![66b4a014b57ee64c304b5c437a1890a5.png](../../_resources/5c0edbc02fe748f2b3a21d23a73d7500.png)









Had issues with cmd typing so changed to power shell.


![6fc72257eee2bde96b325327a6468b3b.png](../../_resources/e0b2b3ba174a49baa1da48b75b0bfa1b.png)

