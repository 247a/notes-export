ur - Bitlab - 10.10.10.114




![bb6ad0f69f60fc0d4d9085700d0cdef1.png](../../_resources/48b8aba4dd6246b68ada67112ba29eff.png)


We have a git lab which requires login



![0d0c6ea252f6b1b2d0554894871a08cb.png](../../_resources/f6ffa11d663e410d9301ec9f981e2433.png)



using the API we can enumerate users unuthenticated. based on this issue https://gitlab.com/gitlab-org/gitlab-foss/issues/40158



![604532b822fa46feb89d2ad4e4455440.png](../../_resources/397d529ca63540aaa0710e765d911554.png)


![0dcdf069c3bf5c7c3e09947ba2282176.png](../../_resources/a14206c2e097468c8c6cd5d4d0ccf56a.png)



![df0f42b98db1349f3c9c7e3df2f97a2d.png](../../_resources/b8872233e41e482999963b7183a9572c.png)





nmap automater found a page


![bbb3e67936b2df403ddbacdbed3ef9fb.png](../../_resources/4d2c36bbfda4413aa64fc52cd7ecd6a5.png)


this page had a top bar with a help page


![7ffa5a6e665715c8f29f439969d440e3.png](../../_resources/cb26118cbfd74cb1a2c684d5ef4e776b.png)


This showed a bookmarks html



![474a793d0999316abddffce64160887c.png](../../_resources/04f45e4b93024361946c39f7fe61a517.png)


![71444e9e62013d436f736883bae61457.png](../../_resources/47fe33ecbc924045aca37f1d5c7bf2ed.png)



The Gitlab login page was a javascript linke. however this was obscured


![8e3b7ad1509674d9b7bccb1747522a4d.png](../../_resources/3ee4f2a5521040e890b382eba25c331d.png)


After unobsucring the code we get

```
document["getElementById"]("user_login")["value"]= "clave";
document["getElementById]"("user_password")["value"]= "11des0081x";
```

Which indicates that this is a login script to login the user we priviously found called clave with the password given.
from this we can login


![e8a2f48eff47ff3cb5b0cc55f3983d9a.png](../../_resources/0e1249bc96d244fa8c0d98dde3bf4bd4.png)


Attempting to go to claves settings we insted get sent to a profile page.


![cd81cceb5b361861861db26921729c33.png](../../_resources/e26ab5897d894004aea8505154535d11.png)


![de092151e10f3fe871d634864dce9530.png](../../_resources/3b45151af6d24f7e83ccca1adec14d43.png)




Looking at the index.php in Administrator/profile
we see 


![0ab17dc2b631bb79f360b4a4cb66af12.png](../../_resources/d3932609b75840d78996f9dd1e9ee730.png)


which indicates that if we add a php backdoor to this repo the hosted site will also gain the back door
Looking in the repo we see a aa.txt file


![e6c91ffcc90d8822ae367a2439e28c3a.png](../../_resources/934a15c74f924822b16a4b12378abff5.png)


there for to test a hypotosis i add a fullstop and merge it to master.


![d0b431c01327b00cf5eab1e4481388a8.png](../../_resources/8fb284ee362d4e648fa6b36358c2c5ed.png)




![7029a5c0cc38a86e8c3563103eb34096.png](../../_resources/09a02d8124f24c138d682664bfd3ddc6.png)



With this we can begin createing a reverse shell, with weevely and upload it.



![b98ad7f72ed65929b9b68948a4e5cfb1.png](../../_resources/596f61897a7b4cc98c385d15f6d97312.png)



![27de464bf968e31d968a48c67b456ad2.png](../../_resources/5a466cf1535e455696028645b3937dba.png)



Now that the shell is uploaded we can connect



![87323a7afac008ee499a526dd59ca467.png](../../_resources/4934119e709f494d91a6b2c8d6630e1e.png)


Continuing to search i found a text document 


![d1d30bbeca1c9af92c66ae20ae48fae4.png](../../_resources/c9bb6f8527a745c8a2ba8f0da0a581b4.png)


using this plus the some of the code from https://www.php.net/manual/en/function.pg-query.php
to make 


![606e5d90060e70acd55fcf907e7a0756.png](:/159b8588b6d1412782bbe648ff391dc3)


we get teh following output


![0298d3c5bb335e3c801bb44c2d1213e5.png](../../_resources/2d5f3ef54d1949b4b49a4f496a297038.png)



So even though the password looks base64 encoded it is not.
(the authore was evil and used a base64 encodes string AS the password)

Once logged in there is a exe in the home area (there are also files from people attempting the box which we can ignore)


![2df330a7a3502e0d38dcdf09f58df6dc.png](../../_resources/0d79d91cc796452283abc0abfc7825d3.png)



Looking at the binary in gihdra there was the string “clave”,
getting the offset and putting a breakpoint (in x64dbg)at the only usage
We see that there is a cmp of a variable against the clave string
Further we see that some ssh credentals are loaded into ebx in the form of an applications connection pramaters
(-ssh root@gitlab.htb -pw "Qf7]8YSV.wDNF*[7d?j&eD4^"


![b77e13637400005e6af84e3da4031ebe.png](../../_resources/040862dfe5b4410d9ba7e327e6b0c31b.png)



Therfor we can ssh in as root


![11ef34bb40d13edafce1493c6cb848a0.png](../../_resources/62204abebd254c078cfe992991398fe0.png)




