ooch - 10.10.10.177

in the nmap scans we see that there is a ftp port
trying anonymous


![328c053dafca34b970551a66d8dce8eb.png](../../_resources/caac363ea6a84ee5ba8cd6e01ddfb5d9.png)


![fba0fefbf5a2e95ca55d13bb7e4e01d6.png](../../_resources/49d66de54ade4629b428ae6abedcd51d.png)




we see we have a text file and download it
it contains the following.

```
Flask -> Consumer
Django -> Authorization Server
```

this may inform us of other parts of the system.

looking at the port 5000 we get


![40440f2e9b07e8a15a27ed7764c413cf.png](../../_resources/684ac881526243ed934e385713bbfcd0.png)


next we can use the register link to register an account


![2c2343849421a7b0963b09d950d2f1af.png](../../_resources/6e2e1b70d1954a499a79c489bd696419.png)


after logging in


![b7c1912b00df2e82af56c3917c31bb6d.png](../../_resources/9d57fc3782ea4824a964c243b3574be9.png)


goting to out go buster scan we see


![3ace91c32e236b535c380feb23a690c6.png](../../_resources/e77a72b0134e4fde81cc05d10bae6ff5.png)


out og the linke there is only one not listed the oauth
navigating to it


![3e7ec5c279252ae42e657e7805ac2231.png](../../_resources/3bebe096cb934e5797d485e4500de2f2.png)



with this we get some domain info.
checking oouch.htb we get our main page same with the consumer vhost.
going back to the project.txt we see Flask -> consumer. which tells me this site may be hosted with flask and an other is django.
This also tells me that python is on the server.


nanivating to the connect page we get a redirect


![1a845b680c61a5dc07d941da06618665.png](../../_resources/2072450f1dc4416183e13d305fcdba51.png)


so another domain.
and a new login on port 8000



![841a8cd8870d373489c49c02a1237f30.png](../../_resources/37bbadc5843d4502af31c2c165f22f0c.png)


navigating to the home page we see


![1cc8a71c4b48b66f3111ab8254dd439a.png](../../_resources/da208b84420442e4abc48cd7f9104671.png)



