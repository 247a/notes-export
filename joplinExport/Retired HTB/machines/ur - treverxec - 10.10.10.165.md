ur - treverxec - 10.10.10.165

performing an nmap scan we find ssh and http


![caf5cc5db9d7554121da063e07a6a81c.png](../../_resources/0c2b7242a60746c688080177a2f694f8.png)


on the http server there is a webpage, there is no diffence between the 10.10.10.165 version and Traverxec.htb


![5cd80ed1f71bf7e11049679133122b52.png](../../_resources/3a5f415b25354752bbf2e28f973b38d6.png)


dirbuster to find files and direcotrys. found nothing usefull bar somthing which shows that the server does not handle spaces in the path properly




![339cf3036fcde7b6eb34d316375a7e5a.png](../../_resources/7b8267497b5040f1a1765f822cfec5fe.png)


![3398b6c792b28f920db6204ba582e12e.png](../../_resources/cf351bba9b4e4cecb442259ac8f8e749.png)


searchsploit 


![e1a2d117c07b9b168f910d5016d7cf8f.png](../../_resources/e9b83c6f00ff4c46a5c960f1fd165df3.png)


Looking at the metasploit version and the sh version they look the same but the sh version is older and i am going to use that instead as i don't want to rely on metasploit

Reading the sh file it looks like i should make a post request which lies about the content length


![9d52ac4efbc09b09fe6d607b45ed4dc0.png](../../_resources/79eb939aac1b4759a7e6f4e261571fff.png)


after that didn't work i googled around and found.
https://www.sudokaikan.com/2019/10/cve-2019-16278-unauthenticated-remote.html

which lead to a py file
https://github.com/sudohyak/exploit/blob/master/CVE-2019-16278/exploit.py

with this we get an rce


When using a number of revers shells the only one thet worked was 




![ee651115b6fb8c6602a5289de7776538.png](../../_resources/6ab4cd15499a40f584f968c53532cd1a.png)



![45aef5336087ee68db64f415cca30509.png](../../_resources/b9d609823a474e829431782dcda23b89.png)




running LinEnum we find


useing john 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
john --wordlist='/home/htb/Desktop/rockyou.txt' htpasswd
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~





![486d2e262d218a3795ee3892df398db8.png](../../_resources/74b628263a344eea9d0190a32ff0e555.png)




![161d2da73b0c4c6ebec919a7bcb6520b.png](../../_resources/79aef9b6c5a5427d81ad81b9e9884c6a.png)



using the directory traversal we can see there is a folder inside david called public_www


![f7635ed5b0508b8afff379cf1a92a04e.png](../../_resources/6548bf3583234283b4372ac02b06db43.png)


When accessed via our shell we fins a direcotry.




![365e5a62e9cede40ca48ffb8cae4bf94.png](../../_resources/bf64a5dd12ea49de9f4208ba44a94276.png)



![251553cf9f637f322077ff1d41bbfe7d.png](../../_resources/7a50c3248d954ba294ccec3b1bdd19ea.png)


using the .htpasswd creds we get in.


![0be7e1a8b34fcd15ff974a0911c2846f.png](../../_resources/6579b68bf5034876b4410253de349bef.png)


So we have a backup of the ssh keys


![1937b4c83161b55f3ff938a03fef3ad1.png](../../_resources/1bdc4ada0fff4211b62d4c7436467aaa.png)


However they are password protected.





![52753b9b50954560094a2812693d7b8d.png](../../_resources/545a98048190495f8055eacc96a9987a.png)


![62389d5fd12708288cd0ad3a6f3d318e.png](../../_resources/734bde6d25ec4e6ab615cbfcdf171639.png)


Now we have user :)


![00c40bed2e8ec9ae82ec6780313e8723.png](../../_resources/8407a5eaaa3a42e0bd56ce2396481b22.png)


So it appers we are in a chroot jail but we can still call /bin stuff.


![42291493a6f5705aa83d39656a5cea5a.png](../../_resources/4313ab442f414248b701d7c37365a0c3.png)


ignoring all the nov items as they are obvesly other peoples attemtps we have a .head file and a .sh file


![f197e1be6c81b3209b7b2a0bd19a306d.png](../../_resources/1fceafc5cde44062805101c6567c16b4.png)


the .head file is not useful


![88c4f696ab0f0ec38b685e6bbc62c025.png](../../_resources/aba1e400201e41259667dad2f8846e8a.png)


However the sh file shows we can run a command as sudo with out password as we can execute teh sh file and get output.



![6f9c6298af8d5d918673ea0625d85b85.png](../../_resources/422efbf26451402e94ef129ddf6c709c.png)



![1dbe1d8c70fcea2656be188118c54d6c.png](../../_resources/bd7c80185e584f81ac47da2f21d08aeb.png)


therfore i attemtpt to use GTFO bins and the call to the journalctl to get a root shell



![436df6a9b06c9e7b4f350fc1ec42a83f.png](../../_resources/329e81e607ef4141bca2155b294c0f47.png)



![3fc0f6e0b0e066c455f8455f7c98182d.png](:/35586e2da6e3475cb562eb350f005a2e)


now we are root


![e660ca31d3176ed23b92386051608c97.png](../../_resources/90277676ae914638b37419491d8338cd.png)








