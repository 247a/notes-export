ur - OpenAdmin - 10.10.10.171

After teh initial nmap scan we find there is a web server


![180a37706ce3ff580a93804a97b309d0.png](../../_resources/251c8425a0f14e3fa7f662919f852cae.png)


Useing dirbuster we find a hidden page


![e692b9ca0a621a1b42ccf257bf880cab.png](../../_resources/13efae1161b8449ebf103deec9b6e07d.png)


which looks like the following


![7acb2cd22ae014ff0aef0b5fa3e903e4.png](../../_resources/0637f44824224ef2aa31ac884eb2bd8e.png)


This howver was a dead end
additonally i found a server called ona


![6e9219be43b884eb21e7110662b53de8.png](../../_resources/5fcffcbf78244b7a95721f4b2ab9b1a3.png)


when using seachsploit we can find an exploit for the machine


![8064e4831bdcf9b7aceb0b9d5d50184b.png](../../_resources/a365f2c8db654e9591237750227eb699.png)


Gaining a copy of the exploit


![98aeb18ac4057edceca2617525130d9a.png](../../_resources/2c7c8b5f2f294dac805c9f7310bac0b6.png)


The file was initially using the windows lineendings and had a comment at the start of the file


![70eb04ba2d772539687760012d7f4b0d.png](../../_resources/892e0290781c4130a3f571043503720b.png)


Once we run this we get a fairly slow shell


![4eab97ccd6d98838ef528d3dffb288f1.png](../../_resources/3ea13d142790467fbe39a78655e5b2b5.png)


Therefor i create a weeverly shell to improve loading times. And send it via a wget


![e09c0b22daa3aba36c7ac1e990a2fdee.png](../../_resources/110a70d790c44a5f851a68a4166c5f0e.png)


![dd93d04d364dcb2d5cf0af914c07cfad.png](../../_resources/c47856bf912649a1a3202b9a6e8d7fb9.png)



![77ce18689ac4fefb38c5a9e06583ef97.png](../../_resources/9decc70dd49247ecb865ba3f358b7b89.png)





Reading though the configs we gain database creds


![10ebc6b35275bafdc4d736c19332265d.png](../../_resources/185056e80d2e408ba3a19c37c57e783f.png)


Grep ing for all mysql services we see jimmy logged in to the mysql service.


![e3dd360b4ee67c5316c0500136cc0f74.png](../../_resources/d422d07f16dc44189a06bad979efab3e.png)


Attempting to use the newly found password via ssh on jimmy we get in.
jimmy:n1nj4W4rri0R!


![818069f2ac122af4fcd93733ab5ff0f6.png](../../_resources/bd59fdfc9edb461eb99d7cb64a8c7d24.png)


after browsing i found an internal site in the sites-enabled


![dc36502d2e1323a8f4866728db4b8067.png](:/e752b9f0417a4c539fb0cb2352d2f17c)


From this we can make a page to dump the rsa key of joanna


![13b87f7116f0dd3a2725cda03eb90933.png](../../_resources/876031849aa74f7cbe881986944edce9.png)



![db8aa901d99b9e7f54172edf27c01629.png](../../_resources/3a9c41f62a5e49a6896d5859647b516b.png)




And we can crack the password on it with john the ripper


![ffb6a9adf79fd4cc7ebf834224874202.png](../../_resources/f492f9f1a56948ef95a0f7db57260004.png)


After setting the correct file permissions we gain access



![c20a1317601c0ec4a395ffb600666e8f.png](../../_resources/0777bda2bc1f47ad821c5a4dc09d1df3.png)


next we can see what commands we can run as sudo


![82aea85de6f8f962b0cdf31b9f806085.png](../../_resources/6065574b7d8c45b5861ee543f5dbd9b5.png)


Looking at gtfo bins we find we can use this to privesc by running nanao as sudo 


![e18432027ed3449f27b7906d41a6d846.png](../../_resources/c9e3a5481137493bafe9745616876eed.png)


pressing ctrl-r to load the read file 


![0c92f116007eb3d50c8954911d56a88f.png](../../_resources/d7c49ca8d75c438ebd298b9bbc010cb8.png)


then ctrl-x to execute a command


![866c0039eef5eb447f17d70ae6c2553c.png](../../_resources/ae8340d55f654e2d9c40e3d2c5cfb0ae.png)



which will be a reset to restor the shell to normal and sh to be an interpreter


![cf2e134a044c3403a31982f5810d091e.png](../../_resources/a09f80d4ae674f60a8aa15de633adfe9.png)



<!-- image missing-->


