ur - forest - 10.10.10.161

**strong text**Nmap


![eafe3e49f5db4992bbaf8c0b298898c2.png](../../_resources/3cf93e261c014198bab062f4ecce7350.png)


Running enum4linux we get some user credentials


![50f8971e1e365e66eccf03b48b9d8322.png](../../_resources/755abf1d6cf94635857d74baf3cc32a6.png)


 
use impacket to get TGT token


![d9010914ca292e62d3f7df82f2b5851f.png](../../_resources/8b80b82158a84ffc9afae3d55bfb6dad.png)


crack password


![9968bae810deab4abda339d390d09927.png](../../_resources/7dbed88f3ee64ef493cdbd33a6e4c8a6.png)


svc-alfresco:s3rvice


![f517b69e7c07121bb97c60af1dffa15b.png](../../_resources/effffaaf9d734dc48d690b40910a19b1.png)


Nest we can pull the Ad infomation with bloodhound


<!-- image missing-->



Using bloodhoud we can query for the shortest path to domain admin


![e6c941385d1d7c3cd75790388074409c.png](../../_resources/e264b6efcf5d427abcf79d982f9e0a6a.png)



now lets use evil-winrm to pull out ps files with us for the privesc


![b63f313a1efcf43a14b90cf7b3fa4167.png](../../_resources/9e00e9dc12e44fb8b195a124ba877438.png)


now lets load powerview 


![e155c57f075948adc7d7a6d40326ecb5.png](../../_resources/3c5e5a44dc16416cb05f0f9d045b4d1d.png)


after floundering i googled the Exchange windows permissions and found the folloing to pull ad hashes using the DCSync which Exchange windows permissions alows us
https://www.theregister.co.uk/2019/01/25/microsoft_exchange_domain_admin_eop/

then i found as a way to complete this attack
https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Active%20Directory%20Attack.md#using-mimikatz-dcsync

next i create a new user
New-ADUser -Name "DONOTUSE_testu" -GivenName "DONOTUSE_testu" -Surname "DONOTUSE" -SamAccountName "testu" -UserPrincipalName "testu@htb.local" -AccountPassword(ConvertTo-SecureString “s3rvice” -AsPlainText -force) -Enabled $true

now add it to the exchange group


![3fbd13e8696fcf56a772b2333901d80c.png](../../_resources/1fc0585bd2774734b8faa44ad6da83e8.png)



next we use ntlmrelayx.py to boost our perms


![2e891407400ef689aad3773767d51da5.png](:/43b1331f8fdd4694b0f0856638f10dc3)



Accessing via the web broser on local host we login as testu



![337791fb87779b07c7cac5419005c1bc.png](../../_resources/a7f67a574f16472c9b13fc58470f0ced.png)



![3b2167f2bf087b36c1f44ba6aede2a55.png](../../_resources/014781a29dd243f7b4607501903618d9.png)




![3a30b0208a15a6285e510d247ae1f75c.png](../../_resources/ed7dc6648bad495b8b07d4cc2fd92131.png)



then running secrets dump as instructed with python3 /usr/share/doc/python3-impacket/examples/secretsdump.py htb.local/testu:s3rvice@10.10.10.161
we get lots of hashes
with the out put of this we can pass the hash


![25d7fb06a67e8d015b69b49b325afda8.png](../../_resources/65c2805aac24494caff7d984175d962c.png)



