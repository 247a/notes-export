Jewel - 10.10.10.211

To begin i ran autorecon to learn what services where running on the box

From this we see ssh and two http servers
![e465fc72d54b6d3133308ea6099c2d88.png](../../_resources/f74cbd771da34b6b8ed4e40662848690.png)
on port 8080 is a blog
![6134b9731dc27e7cea4e6e8ed15c2c17.png](../../_resources/f97faffa24e54fcf8f24d1119894d8c2.png)
Navigating to an invalid page we see
![4f378c36cfc16034eccafd9fbd191fbd.png](../../_resources/30800051f84047f29c2629e99f0bdccf.png)
This is useful as it gives us some intel


next i attempted to register a user to see what is behind the login page
![172fcff2a091d2deed61d828605e040f.png](../../_resources/31e8ffe6dcd64ecb876c0423f4bdd1b7.png)


Next on port 8000 we see that there is a git server
![fb06965be3113d513244add1ff017de0.png](../../_resources/4a243254f2c94cedb2242292162607d1.png)
At the same time the name of the blog is one of the repos, this likely means we can look at the source.

Also the web server on port 8080 is running with nginx and an interesting plugin.
![db70662a861d066625c6aa5d8c550280.png](../../_resources/2bcf2ef79ebf4d139e807db6e1c550e0.png)

looking at the source it appers there is a redis server 
```
production:
  adapter: redis
  url: <%= ENV.fetch("REDIS_URL") { "redis://localhost:6379/1" } %>
  channel_prefix: blog_production
```
Stuck.....