ur - registry - 10.10.10.159

After an initial nmap scan we see a http and https server


![13f7564c5ed55c02554b51d78c196174.png](../../_resources/f445bb2c8a554239a17abe944bd027e5.png)


however nothing good appers to be usefull
however when we look at the sslscan results we find that there is an extra subdomain


![d2af63ff130cbe4b2d1744979b0319c7.png](../../_resources/0a9a3ba60d364728813acb7997411426.png)


This name make me think of docker registerays. which are http services for hosting docker continer images


![faf461d99e69699de315f2df8fa18323.png](../../_resources/e88f14777a8f4a528988d787c780803c.png)


When a request is made to the version check we get a password promt.


![bb369122a0a07afce775e572e8ec6267.png](../../_resources/477fbbf65d5d4bb6bb10dd7c1cff8e8e.png)


Using the username admin and password admin we are greeted with the following


![36b532c9ec3c6461722c22de79099c0c.png](../../_resources/bb1a609e461c46e0b9f1578411ea8367.png)


Next we can use the api to list the repositories with <host>/v2/_catalog


![67cbf02f94a3af16e737cfdd68cc5ecb.png](../../_resources/3a0ff7864482479d9eb460e9a71a52ee.png)



we only have one image therfor we can list the tags on the image in  the form <host>/v2/<image>/tags/list


![decd0bfbff3bb0af56af7d5ad9dd09dc.png](../../_resources/a53e29c496f54298a2815c11d1c48fda.png)


Next we can get the manifest for the tag, which means list the tar.gz files that make up the image. This is done with <host>/v2/<image>/manifests/<tag>


![b39affe9a53739b5c22d0a64c83231b5.png](../../_resources/eac2ecbd4e4b4b0faf96eeb117219150.png)



![c3aa99018ee0a0c61dc912d0450e4c0a.png](../../_resources/e143afd13fb244a0bba5f72f2d1e0b3e.png)



From this we can download each blob with <host>/v2/<image>/blobs/<blobSum>


![b6158cee4497d360cbad7c917887eff1.png](:/cfc61d559cbd4dd39c42885179d624f1)



After downloading all the blobs, extracting them and revieing the files only a few looked interesting


![ee688e6b7a6bab0e3915d7135ecc85d3.png](../../_resources/5b8a570812804609a066c8420348b96b.png)


The first observation is that in the shadow- file there is a user mentioned(bolt), but removed from the shadow file.



![723c996296e93f3d8f81dbadaea8916b.png](../../_resources/f7905cc59a7641678645ad2cd811eb79.png)




the next interesting observation is the 01-ssh.sh file
![ad2744fa194d4dc775432cadff15016f.png](../../_resources/faa7acde6fce46979f162ff5170072e7.png)

This looks like a password(GkOcz221Ftb3ugog) for the id_rsa file
therefor when attepting to ssh we can use this id_rsa file and the password as bolt to gain access.


![fe294b08a3589768caa89eb86ed980c8.png](../../_resources/0412cfeee6694db0ab73d979d9dcc4ad.png)


After browsing the files i found this


![1a232bd0b21742bb0aa6a26b7136a4bf.png](../../_resources/c318552866df4ba9996766d347819b7c.png)


This means that www-data(which is the use the web service is running can likely run sudo with no password)
As it it php i fall back to the trusty weevely
 

![87811e7c89686130f7c350c7a9eb0d37.png](../../_resources/bff65edd6b474451886644198a760250.png)


however when checking the perms of the directory i do not have permission.
But looking in the bolt directory we find a web app. and in there there is a database.


![097fc97b1028779a2dd8573ccb162212.png](../../_resources/86778af393b04d128199106cc5f270f5.png)


loading the database we can see a username email and password hash


![ef42f8370e385a096e08c2449515e389.png](../../_resources/6917e1d49ed14533b9525404c5552d98.png)


Passing the hash to john the ripper we get the password


![a83e51f27f961f0f63fed9c775142bd7.png](../../_resources/b7d7cfa237df4c86a07a52216c1edfb4.png)


reseaching bolt i foun that the admin page is normaly in the bolt directory, but as the web app is in a bolt directory then we go to bolt/bolt


![56a314de2c88c7106ecb07637c9eceb5.png](../../_resources/7d4379db81d849b3bd2fd02a4b4aefb9.png)


and login with admin:strawberry


![b7bfaa6c52823d0e07398e4386aa0ae6.png](../../_resources/8d4146e1cc67448a9e2e198a120523a0.png)



Looking for an exploit i found the following
https://www.exploit-db.com/exploits/46664

however due to how it operates we have to use install the following firefox extension to bypass the cross sight scripting protections
https://github.com/ThomazPom/Moz-Ext-Ignore-X-Frame-Options

As there is a script to remove our shell regularly which is created from the exploit
we can create a longer term shell by base64 encoding the script, storing the encoded script in /tmp/6.txt
then replace the command to \\\"base64 -d /tmp/6.txt \\x3e /var/www/html/w.php \\\"


![f29ab6ad58c1943cb29e1add8ee713ff.png](../../_resources/fb2429f3da564b1b9740de0c9102f41d.png)


![74a2afe2b756926569e4751af382bcbc.png](../../_resources/73b28e7520f44160bbc5b4076e894c35.png)




From this we can now use our weevely shell to get the console as www-data


![a50e8f021c6edadf06d109b3c6f97a0b.png](../../_resources/fcc08177a2f643469e92e7d36d4322ce.png)


Useing sudo -l we can see what commands www-data can run as root


![d6634ae335329781710d913ec8dd612f.png](../../_resources/809c1766524f42e2afec8e8b32045515.png)



Reading the restic docks we can see that we can start a backup server with 

<!-- image missing-->


but when running the backup command i saw that there was no need for the restic abckup server as it cats out teh files backedup
Therfor if we run sudo /usr/bin/restic backup -r rest: --files-from /etc/shadow we get the shadow file and could crack teh root pass. after removing the additionall rubiish data


![20574ecc35935210c036e5f7fd02db30.png](../../_resources/cfb36e7518254b4ca8275b139922924c.png)


or we can just read the root flag


![7a2ef1af8b84a31a7a92b4b09f440ae2.png](../../_resources/35332b412bf64e9e924497b26c54bec8.png)


equaling ntrkzgnkotaxyju0ntrinda4yzbkztgw



