Omni

Performing an auto recon scan we see that port 8080 is open as well as port 135.
![47dfe8c2d4df18d347b54f97c8ca22ac.png](../../_resources/b9e54d3a59fd4abf8b491f4c39a9adb8.png)
Navigating to the page with firefox over http on port 8080 we see a login request for a Windows device portal.
![cb336aea444b65c178a9c3f75b104f43.png](../../_resources/caaecae9f7eb41a0b8e3833e1d3c8456.png)

