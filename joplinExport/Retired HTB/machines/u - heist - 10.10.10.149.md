u - heist - 10.10.10.149

running an nmap scan we see we have a few ports open one of which is a web server



![fb25573b6f1da2b3379cf347d43b68dd.png](../../_resources/156694b298da4084aa41e63d2d870edb.png)


So on port 80 we have a website.


![52a32199c9d723cbb74f0b84c2b14f41.png](../../_resources/6bf41c1f513047789dbb8920686b9c66.png)


if we click on login as guest we get 


![eb941e147e587120d44ae63e3bdaffe7.png](../../_resources/d9c48ad640574fd4b042b7b69a8dfc1c.png)



Using john the ripper jumbo, we can extract 2 passwords and a hash to use with john the ripper



![6e676043b1488e6082706786cbc50c7f.png](../../_resources/611394d5fc2e4be0976540fad43c3559.png)



![e2861c2218dd87d24d85af278dbfef29.png](../../_resources/ba97888b25524ad6bc006ff8e84ad518.png)


So we have a feeling that Hazzard has an account due to this message


![dd401f1555e42d577212c9dadce669bf.png](../../_resources/4f3ac0fdde0e41d0b61aebdd320f47eb.png)


Attempting the three passwords in the login page yeilded nothing.
As the nmap scan shows nmap, next i will use enum4linux to attempt to enumerate the smb service
However we get told


![d4cfcff7211d72775720aad203887426.png](../../_resources/56f8971bc5c84ee6ac3fb4531009d91e.png)


Hence attempting the username hazard and the three passwords gained, we find that the user hazard has the password stealth1agent



![83951f191772b97daef1b6e7559fa798.png](../../_resources/aab8ee6805214288a75d285cd61a6e59.png)


From this we can see the initial workgroup that hazard is a member is "WORKGROUP"
Hence running “enum4linux -u hazard -p stealth1agent -w WORKGROUP -a 10.10.10.149” we get more output
From this we find
 

![88a7a0520013196d8059c2068115f2f8.png](../../_resources/e721d4a1f0134f3c88a82380f046f7c8.png)



![ab2e10469b9cbc0c4b7146ef4b7f6b37.png](../../_resources/0b25c7347d5a4078b6bb387de449d3f9.png)



![38287fcd56d55f5d3d8c5de65f43145d.png](../../_resources/b15cae46d37c462ab497b53feeb6d99d.png)


![19597ff5f48953541e67f523b9e2ad57.png](../../_resources/c5045812f5054b1bb2c0f6dcec5600de.png)







From this we have found that the acctuall work group is SUPPORTGROUP, and that there are the users “Administrator” “Guest” “DefaultAccount” “WDAGUtilityAccount” “Hazard” “support” “Chase” “Jason”


![331471a7b08ef2881fbae53763fc5506.png](../../_resources/779be6063385415ba01a7e0492eb2572.png)





From here we can use evil-winrm to get remote code execution on the winrm service using the wsman port.
using hazard we get auth fail


![63afad849cdf65d7a6ae752c9e93e2f1.png](../../_resources/8e518baaf2164c95a05889a50e25ef1f.png)


![d951f0bf9efc4173d940cffae5ad7b74.png](../../_resources/5813b40c9fc14b9989ba3fdcb55280a2.png)




in chase on the desktop we see a todo.txt


![7655a439e832f8cd00d7f69ca8d108f4.png](../../_resources/84e43f258a35433185aebdba78873a97.png)





