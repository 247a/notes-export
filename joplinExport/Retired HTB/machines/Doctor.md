Doctor

IP: 10.129.2.21

To begin i ran autorecon this instantly found the found ports 22,80, and 8089
![ce08552ec1550cbe8a4235165ea48da9.png](../../_resources/5c4280b491de413ca7599bd056f4e654.png)

Nagigating to port 80 we see that it appers to be a doctors site
![fc9f1a20654abb1b6585c92920b4fa0a.png](../../_resources/dea8fd7d7bab4935bbf1b3fa2f0ff24c.png)

we also get an email with a domain[info@doctors.htb]
![02f1458c457ae724a966fbfb54052d3d.png](../../_resources/fb5bc425c0a64d36b43d79193eb306e8.png)
We can add this to our hostfile.

looking at port 8089 we see a splunkd service
![abf4c5577b171430e68b306556edf934.png](../../_resources/9e91712608794cfea5058a38a1cf601b.png)
looking on the splunk website we can see that the default credentials may be admin and changeme
![642dd8c2dbeb4a1655f4f22ccc4ad068.png](../../_resources/3d8542bb14064ffc98cd01f328675d92.png)
