ur - Monteverde - 10.10.10.172

With nmap we can see that there is the smb port open.
Running enum4linux we get a list of users


![186e424756e0b74a12452f5867127356.png](../../_resources/51c6660ddabd4ef7b9007fc5a099378a.png)



we also see the name of the domain server (MEGABANK.local)


![8cd2896b707f461b635ee4fa01069513.png](../../_resources/9ead63fd2872427fb37ea2313b441e9c.png)



dumping these into a texdocument stating all the users we can use creackmap to see if we can get some creds, normally use hydra


![d1e0cc0b92fa7de79dc5c8b7268604a6.png](../../_resources/5929f71235164311b7407f4a5f685f06.png)



MEGABANK\SABatchJobs:SABatchJobs 

with this we can login with SMB


![b9a54cc88be71474d3015d5e0432c20a.png](../../_resources/4137add90282404c9cdae3e888f7e5f3.png)


from this i found a useful file


![ec709b28c71b7874db85d239c6af5cf3.png](:/7cdf972e6fb34511bd0781e59990fdb0)


which looks like it has cresd in it


![efaec2ef6a90f73f19f9321eb7bf6e60.png](../../_resources/ec7328bee57840e386a507d9274c7eb6.png)


logging in with evil winrm we get a shell


![29441cd16a0651a7121eec751d4db48f.png](../../_resources/f15f99c4df5f4296ae8086325742866e.png)



Looking at this link gave
https://blog.xpnsec.com/azuread-connect-for-redteam/


![d88d680ef15dc8e571c4caa58d207056.png](../../_resources/59be31d53c7d45029b53a4814a798e46.png)



Tryeing the new passwords on Administrator we get


![4c629c97ebb93b626b4bde7ba5694e07.png](../../_resources/0e77ee58b65c40ff8126c01f418e0c14.png)


