evil-rm

## URL

https://github.com/Hackplayers/evil-winrm

## Install
```sh
sudo gem install evil-winrm
```


## Use
```sh
~$ evil-winrm  -i 192.168.1.100 -u Administrator -p 'MySuperSecr3tPass123!' -s '/home/foo/ps1_scripts/' -e '/home/foo/exe_files/'
```