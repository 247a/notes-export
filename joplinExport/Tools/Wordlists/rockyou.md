rockyou

## Install

```sh
sudo apt install wordlists --reinstall
```

## Extract
```sh
gzip -d /usr/share/wordlists/rockyou.txt.gz -c > ~/Desktop/rockyou.txt
```
