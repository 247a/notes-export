radare2

## URL
https://www.radare.org/n/

## Install
```sh
git clone https://github.com/radare/radare2.git
cd radare2 
./sys/install.sh
```
## Update
```sh
./sys/install.sh
```

## Uninstall
```sh
 make uninstall
 make purge
```

## Gain info about app
```sh
rabin2 -I <app>
```
## Load bin
```sh
r2 <app>
```
## Analyze bin

in r2 console run aaa and press enter
this outpus somthing simmiler to 


![aa78f57d5be742ec38d8d2442aa99c86.png](../../../../_resources/895f4e51c13c438f9cf986d9a2c1c9da.png)

## Hex mode 
press shift + V + enter on r2 console to enter hex view.
press n to move to nextet section

## Seek to function
in r2 console
```
s <location/name>
```

## Visual Mode
press v + enter on r2 console to enter Visual view.