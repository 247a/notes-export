radare2-webui


## Install

First, you should install radare2, then r2pm will handle this for you:
```sh
$ r2pm -i www-enyo
$ r2pm -i www-m
$ r2pm -i www-p
$ r2pm -i www-t
```
This process will install the proper UI by downloading the latest version available.





## Use it

You can run one of the UI by typing the following command with: enyo, m, p and t.
```sh
$ r2 -q -e http.ui=<UI> -c=H /bin/ls
```
## Uninstall

To uninstall an UI, you can use this command.
```sh
$ r2pm -u <package>
```