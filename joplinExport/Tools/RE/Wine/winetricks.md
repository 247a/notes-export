winetricks

# Dot net
Use wineticks to make new prefix (call it wine32)
Install dotnet in prefix with the install components option.

Run wine as ```WINEPREFIX=~/.local/share/wineprefixes/wine32/; wine [wine app]```