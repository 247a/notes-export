wfuzz

wfuzz — https://github.com/xmendez/wfuzz


Can be used to find prameters

- –hh (filter the length of characters in source code)(aka the error message)
- -c   (Output with colors)
- -w   (Wordlist)
- FUZZ (FUZZ keyword will be replaced by the word from the wordlist)

E.G for a GET Request
```sh
wfuzz --hh=24 -c  -w /usr/share/dirb/wordlists/big.txt http://<ip>/api/action.php?FUZZ=test
```



- --hs (ignore responces with the following content)
- -d (content of post request)
- -z (Scan mode (Connection errors will be ignored).)

E.G. for a post request
```sh
wfuzz -c -z file,/root/Documents/MrRobot/fsoc.dic — hs incorrect -d “log=FUZZ&pwd=test” http://192.168.240.129/wp-login.php
```