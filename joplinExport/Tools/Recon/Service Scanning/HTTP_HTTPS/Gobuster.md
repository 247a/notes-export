Gobuster

GoBuster — https://github.com/OJ/gobuster
## Install
```sh
sudo apt install gobuster
```

## Example usage
```sh
gobuster dir -u example.com -w /usr/share/wordlists/dirbuster/directory-list-2.3-medium.txt
```
## Options

| Option | Meaning |
| --- | --- |
| -u <string> | url |
| -w <string> | Wordlist path |
| -p <string> | Proxy to use in dir mode |
| -m <dir/dns> | mode. “dir” = Directory/file; “dns” = DNS |
| -c <string> | cookies to use in requests. |
| -U <string> | Basic Auth user |
| -P <string> | Basic Auth pass |
| -a <string> | useragent |
| -s <string> | Positive status codes (default "200,204,301,302,307") |
  

