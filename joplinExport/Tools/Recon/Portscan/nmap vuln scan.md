nmap vuln scan

## Install
```sh
cd /usr/share/nmap/scripts/
git clone https://github.com/scipag/vulscan.git
```

## Update
```sh
cd vulscan/utilities/updater/
chmod +x updateFiles.sh
./updateFiles.sh
```

## Use
```sh
nmap --script vulscan -sV -p# ###.###.###.###
nmap --script vulscan --script-args vulscandb=database_name -sV -p# ###.###.###.###
nmap --script vulscan --script-args vulscandb=scipvuldb.csv -sV -p# ###.###.###.###
nmap --script vulscan --script-args vulscandb=exploitdb.csv -sV -p# ###.###.###.###
nmap --script vulscan --script-args vulscandb=securitytracker.csv -sV -p# ###.###.###.###
```