autoRecon


## URL
https://github.com/Tib3rius/AutoRecon

## Install depenaces


```sh
sudo apt install curl enum4linux gobuster nbtscan nikto nmap onesixtyone oscanner seclists smbclient smbmap smtp-user-enum snmp sslscan sipvicious tnscmd10g whatweb wkhtmltopdf
```

## Install
```sh
sudo python3 -m pip install git+https://github.com/Tib3rius/AutoRecon.git
```

## Use
```sh
python3 autorecon.py 10.10.10.158
```