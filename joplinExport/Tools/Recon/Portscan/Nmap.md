Nmap

## Options
- -sC for defualt scripts
- -sV enumerate version
- -oG Grep format
- -oA all format
- -p0- scan all ports (including 0)




## Initial Scan
nmap -p0- -oA nmap-tcp.txt<ip>



## Further Scan
nmap -sC -sV -oA nmap-tcp-service.txt  -p<ports><ip>


## Vuln Scan
nmap --script=*-vuln* -sV -oA nmap-tcp-vuln.txt -p<ports> <ip> 


## UDP Scan
namp -sU -p0- -oA nmap-udp.txt <ip> 