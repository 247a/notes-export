pwntools

# Intro

pwntools is a Python3 lib/framework to help with finding and exploiting bufferoverflows


# CMDs
## pwn cyclic
### What is it for 
command line command sends data, used for fuzzing. Basically msfvenom genpattern and checkpattern.

### How to use



#### Cause crash
> pwn cyclic [numchars] | [your bin]

Get a crash/segmentation fault

> dmesg

Check for "segfault at [memlocation]" (if me is 0 increase by 1 or 2)
If we get general protection error then reduce buffer size.

#### Get offset
>pwn cyclic -l [crashvalue] [numchars]


#### flags
-a = alphabet (defaults to all lower case letters)
-l <lookup_value>= lookup a crash position
-n <length> = Size of the unique subsequences(bittness) (defaults to 4)
-c <supported val> =The os/architecture/endianness/bits the shellcode will run in

-h = help



# Code