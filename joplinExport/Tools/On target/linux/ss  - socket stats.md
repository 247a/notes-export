ss  - socket stats

# Purpose
`ss` is used to dump socket statistics. It allows showing information similar to netstat. It can display more TCP and state informations than other tools. 

# `ss -anp`
Displays all sockets, it does not attempt to resolve the sertvice name and shows the proccess useing the socket.

# `ss -anp | grep <pid>`
Displays all sockets, it does not attempt to resolve the sertvice name and shows the proccess useing the socket. And filter for pid

# `ss -lnpt | grep <pid>`
Show listening tcp sockets it does not attempt to resolve the sertvice name and shows the proccess useing the socket . And filter for pid