Overview

# /etc
Contains config files

# /proc
Contains info on running procceses, such as the working directory, and the inputs and outputs.

# /sbin
system Bins that require root.

# /bin
system bins that often do not require root

# /opt
convention for custom apps not installed via a repo.

