chrontab - run events on timer

# Format
format = `* * * * * command`
Where the stars mean in order:
`minute` `hour` `day of month` `month` `day of week(0 | 7 = sunday)`
If a `/` is used that means the step so if `*/5 * * * * command` was used it would execute the commadn every 5 minutes.
A comma means times so `20,30 * * * * command` would fire at 20 past and half past every hour.
a dash is an equivilant to a range so `5-7` is the same as `5,6,7`

# Add task
It is possible to edit the chrontab for a user with 
`chrontab -u <username> -e` 
if the user name is not set it defualts to the current user.
## Notes on adding to task
If there is any output then it is sent to the local email account.
Hence it is a good idea to add `>/dev/null 2>&1` to the end of the command which will disable all output.


# List tasks
`crontab -u <username>  -l`
if the user name is not set it defualts to the current user.

# Flush crontab
`chrontab -r`

# Key locations
Path | desc
----- | -----
/etc/crontab | root crontab
/etc/cron.d | 	Place all the scripts here and call them from /etc/crontab file
/etc/cron.daily	| Place all the scripts here to run once a day
etc/cron.hourly |	Place all the scripts here to run once an hour
/etc/cron.weekly |	Place all the scripts here to run once a week
/etc/cron.monthly |	Place all the scripts here to run once a month


