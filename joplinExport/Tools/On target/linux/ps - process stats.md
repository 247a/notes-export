ps - process stats

# Purpose
Lists info about the current running proccesses.


# `ps -aef --forest`
This command shows a tree of the call chart. Such that you can see the proccess higherarchy.

Usefull for attack path analysis.

# `ps aux` | `ps -e`
To see every process on the system using BSD syntax
Should use `ps -e` instead

# `ps -U root -u root`
To see every process running as root (real & effective ID)

