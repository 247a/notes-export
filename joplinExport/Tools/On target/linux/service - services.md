service - services

# `service --status-all`
Service info

# `service <service name> start`
Start service

# `service <service name> stop`
Stop service

# `service <service name> restart`
Restart service


# Create a service
Create a file in `/etc/systemd/system/`
containing
```
[Unit]
Description=ROT13 demo service
After=network.target
StartLimitIntervalSec=0[Service]
Type=simple
Restart=always #
RestartSec=30 #wait 30 secons to restart
User=centos
ExecStart=<command>

[Install]
WantedBy=multi-user.target
```