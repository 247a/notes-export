Apache

# Common Config locations
`/etc/httpd/conf/httpd.conf`
`/usr/local/etc/apache22/httpd.conf`
`/usr/local/etc/apache2/httpd.conf`
`/etc/apache2/apache2.conf`

# Common Site locations

# Log locations
## Common Log locations
### Access logs
`/var/log/httpd/access_log` : RED HAT
`/var/log/apache2/access.log` : Debian
`/var/log/httpd-access.log` : BSD
### Error logs
`/var/log/httpd-error.log` : RED HAT
`/var/log/apache2/error.log` : Debian
`/var/log/httpd/error_log` : BSD

## Config option 
`CustomLog` : Access log
`ErrorLog` : Error Log


# Blue team
## `grep 404 <log file name> | grep -v -E "favicon.ico|robots.txt"`
Looks in the apache logs for 404 errors

## `head access_log | awk '{print $7}'`
Prints apache logs for requested files

## `watch -n 300 -d ls -lR <webfolder>
Checks for new files every 5mins.

## `cat <log file> |