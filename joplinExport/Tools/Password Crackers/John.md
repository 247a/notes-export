John

## URL
https://github.com/magnumripper/JohnTheRipper
https://github.com/magnumripper/JohnTheRipper/blob/bleeding-jumbo/doc/INSTALL-UBUNTU

## Install dependacies
```sh
sudo apt-get -y install yasm pkg-config libgmp-dev libpcap-dev libbz2-dev
sudo apt-get -y install git build-essential libssl-dev zlib1g-dev
```


## CPU

```sh
sudo apt-get -y install ocl-icd-opencl-dev opencl-headers pocl-opencl-icd
```

## Install jumbo version

```sh
git clone https://github.com/magnumripper/JohnTheRipper.git
cd ./JohnTheRipper/src
./configure && make
cd ..
cd ./run
```
## PDF cracking
requires john the ripper jumbo
```sh
pdf2john.pl <pdf> > pdf.hash
john pdf.hash
john --show pdf.hash
```
## Single wordlist
```sh
john --wordlist='/home/htb/Desktop/rockyou.txt' --rules
```
## All wordlists
could take a long ass time

```
find /usr/share/wordlists/ -follow -type f -name '*' -not -name '*.gz' -exec cat {} \; 2>/dev/null | ~/Desktop/tools/PasswordCrackers/JohnTheRipper/run/john --pipe --rules hash
```
